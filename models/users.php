<?php
  class Users {
    // we define 3 attributes
    // they are public so that we can access them using $post->author directly
    public $id;
    public $author;
    public $id_std;

    public function __construct($id, $author, $id_std) {
      $this->id      = $id;
      $this->author  = $author;
      $this->id_std = $id_std;
    }

    public static function add($user_u,$user_add)
    {
      $db = Db::getInstance();
      $id_user = intval($user_u);
      $id_add = intval($user_add);
      $req = $db->prepare('SELECT * FROM friends WHERE users_add = :id_user AND users_admit = :id_add');
      $req->execute(array('id_user' => $id_user, 'id_add' => $id_add));
      $post = $req->fetch();
      if(is_array($post) || $user_u == $user_add)
      {
        return 0;
      }else
      {
        $sql = $db->prepare("INSERT INTO `friends` (`users_add`, `users_admit`,`status`) VALUES ('$user_u', '$user_add','2')"); 
        $sql->execute();
        return 1;
      }
    }

    public static function alert($id)
    {
      $list = []; 
      $list_find = [];
      $db = Db::getInstance();
      $id = intval($id);
      $req = $db->prepare('SELECT * FROM friends WHERE users_admit = :id');
      $req->execute(array('id' => $id));
      foreach($req->fetchAll() as $add) {
        var_dump($add);
          if($add['status'] == 2)
          {
            $list_find[] = $add['users_add'];
          }
      }
      if(is_array($list_find))
      {
        foreach($list_find as $i)
        {
            $id = intval($i);
            $sql = $db->prepare('SELECT * FROM members WHERE id = :id');
            $sql->execute(array('id' => $i));
            foreach($sql->fetchAll() as $user) {
                $list[] = new Users($user['id'],$user['username'],$user['student_id']);
            }
        }
      }
      return $list;
    }

    public static function all()
    {
      $list = [];
      $db = Db::getInstance();
      $req = $db->query('SELECT * FROM members');
        foreach($req->fetchAll() as $post) {
        $list[] = new Users($post['id'], $post['username'], $post['student_id']);
      }
      return $list;
    }
    public static function confirm($id_u,$id_a)
    {
      $db = Db::getInstance();
      $id_u = intval($id_u);
      $id_a = intval($id_a);
      $req = $db->prepare('UPDATE friends SET status = 1 WHERE users_add = :id_a AND users_admit = :id_u');
      $req->execute(array('id_u' => $id_u, 'id_a' => $id_a));
      return true;
    }
    public static function delete($user_u,$user_add)
    {
      $db = Db::getInstance();
      $id_user = intval($user_u);
      $id_add = intval($user_add);
      $req = $db->prepare('SELECT * FROM friends WHERE users_add = :id_user AND users_admit = :id_add OR users_add = :id_add AND users_admit = :id_user');
      $req->execute(array('id_user' => $id_user, 'id_add' => $id_add));
      $post = $req->fetch();
      if(is_array($post) || $user_u == $user_add)
      {
        $req = $db->prepare("DELETE FROM `friends` WHERE users_add = :id_user AND users_admit = :id_add OR users_add = :id_add AND users_admit = :id_user");
        $req->execute(array('id_user' => $id_user, 'id_add' => $id_add));
        return 1;
      }else
      {
        return 0;
      }
    }
    public static function find($id_std) {
      $db = Db::getInstance();
      $id = intval($id_std);
      $req = $db->prepare('SELECT * FROM members WHERE id = :id');
      $req->execute(array('id' => $id));
      $post = $req->fetch();
      // we create a list of Post objects from the database results
        return new Users($post['id'], $post['username'], $post['student_id']);
    }

    public static function is_friend($user_u,$user_add)
    {
      $db = Db::getInstance();
      $id_user = intval($user_u);
      $id_add = intval($user_add);
      $req = $db->prepare('SELECT * FROM friends WHERE users_add = :id_user AND users_admit = :id_add OR users_add = :id_add AND users_admit = :id_user');
      $req->execute(array('id_user' => $id_user, 'id_add' => $id_add));
      $post = $req->fetch();
      if(is_array($post))
      {
        if($post[3]!= 2)
        {
          return 1;
        }
        else
        {
          return 2;
        }
      }else
      {
        return 0;
      }
    }
  }
?>