<?php
  class Group {
    // we define 3 attributes
    // they are public so that we can access them using $post->author directly
    public $id;
    public $sec;
    public $name;

    public function __construct($id, $sec, $name) {
      $this->id = $id;
      $this->sec  = $sec;
      $this->name = $name;
    }
    public static function add($sec,$subject){
      $db = Db::getInstance();
      $id = intval($sec);
      $sql = $db->prepare("INSERT INTO `groups` (`Section`, `Name_Sec`) VALUES ('".addslashes($sec)."','".addslashes($subject)."');"); 
      $sql->execute();
      $id = intval($sec); 
      $req = $db->prepare('SELECT * FROM groups WHERE Section = :id');
      $req->execute(array('id' => $sec));
      $post = $req->fetch();
      return new Group($post['id'], $post['Section'], $post['Name_Sec']);
    }
    public static function into($sec,$subject) {
      $db = Db::getInstance();
      $id = intval($sec);
      $req = $db->prepare('SELECT * FROM groups WHERE Section = :id');
      $req->execute(array('id' => $sec));
      $post = $req->fetch();
      if(!is_array($post)) {
        return Group::add($sec,$subject);
      }else
      {
        return new Group($post['id'], $post['Section'], $post['Name_Sec']);
      }
    }
  }
?>