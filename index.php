<?php
include_once 'includes/db_connect.php';
include_once 'includes/functions.php';
sec_session_start();
?>
<?php
  require_once('connection.php');

  if (isset($_GET['controller']) && isset($_GET['action'])) {
    $controller = $_GET['controller'];
    $action     = $_GET['action'];
  } else {
  	header('Location: http://localhost/SocialNetwork/?controller=posts&action=index&id_u='.$_SESSION['user_id'].'');
  }

  require_once('views/layout.php');
?>