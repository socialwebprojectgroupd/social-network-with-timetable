<?php
  function call($controller, $action) {
    require_once('controllers/' . $controller . '_controller.php');

    switch($controller) {
      case 'pages':
        $controller = new PagesController();
      break;
      case 'posts':
        // we need the model to query the database later in the controller
        require_once('models/post.php');
        require_once('models/comments.php');
        require_once('models/users.php');
        $controller = new PostsController();
      break;
      case 'comments':
        require_once('models/comments.php');
        $controller = new CommentsController();
        break;
      case 'groups':
        require_once('models/users.php');
        require_once('models/groups.php');
        $controller = new GroupsController();
        break;
      case 'users':
        require_once('models/users.php');
        $controller = new UsersController();
        break;  
    }

    $controller->{ $action }();
  }

  // we're adding an entry for the new controller and its actions
  $controllers = array('pages' => ['home', 'error'],
                       'posts' => ['index', 'show','confirm'],
                       'comments' => ['insert'],
                       'groups' => ['index','confirm'],
                       'users' => ['profile','add','delete','confirm']);

  if (array_key_exists($controller, $controllers)) {
    if (in_array($action, $controllers[$controller])) {
      call($controller, $action);
    } else {
      call('pages', 'error');
    }
  } else {
    call('pages', 'error');
  }
?>