<?php
// get page from web reg cmu
$file = file_get_contents('https://www3.reg.cmu.ac.th/regist259/public/result.php?id=580611051');
$arr_s=[];
$arr_c=[];
$arr_sec=[];
$arr_days=[];
$arr_t=[];
$doc = new DOMDocument("1.0", "utf-8");
@$doc->loadHTML(mb_convert_encoding($file, 'HTML-ENTITIES', 'UTF-8'));
$doc->preserveWhiteSpace = false;

// get subject
foreach ($doc->getElementsByTagName('tr') as $tr) {
    if($tr->getAttribute('class') == 'msan8' ){
        foreach($tr->getElementsByTagName('td') as $td){
            if($td->getAttribute('width') == '157'){
                $arr_s[]=$td->nodeValue;
            }
        }
    }
}
// get code
foreach ($doc->getElementsByTagName('tr') as $tr) {
    if($tr->getAttribute('class') == 'msan8' ){
        foreach($tr->getElementsByTagName('td') as $td){
            if($td->getAttribute('width') == '57'){
                $arr_c[]=$td->nodeValue;
            }
        }
    }
}
// get sec
foreach ($doc->getElementsByTagName('tr') as $tr) {
    if($tr->getAttribute('class') == 'msan8' ){
        foreach($tr->getElementsByTagName('td') as $td){
            if($td->getAttribute('width') == '35' && $td->getAttribute('bgcolor') == '#FFFFFF'){
                $arr_sec[]=$td->nodeValue;
            }
        }
    }
}
// get days
foreach ($doc->getElementsByTagName('tr') as $tr) {
    if($tr->getAttribute('class') == 'msan8' ){
        foreach($tr->getElementsByTagName('td') as $td){
            if($td->getAttribute('width') == '35' && $td->getAttribute('bgcolor') == '#E3F1FF'){
                $arr_days[]=$td->nodeValue;
            }
        }
    }
}
// get time
foreach ($doc->getElementsByTagName('tr') as $tr) {
    if($tr->getAttribute('class') == 'msan8' ){
        foreach($tr->getElementsByTagName('td') as $td){
            if($td->getAttribute('width') == '62'){
                $arr_t[]=$td->nodeValue;
            }
        }
    }
}
//var_dump($arr_s); // arr subject 
//var_dump($arr_c); // arr course
//var_dump($arr_sec); // arr sec
//var_dump($arr_days); // arr day
//var_dump($arr_t); // arr time

?>
    <body>
        <table class="timetable" cellspacing="0" cellpadding="0">
            <tr class="time head">
                <td rowspan="2">Time</td>
                <td>08</td>
                <td>08</td>
                <td>09</td>
                <td>09</td>
                <td>10</td>
                <td>10</td>
                <td>11</td>
                <td>11</td>
                <td>12</td>
                <td>12</td>
                <td>13</td>
                <td>13</td>
                <td>14</td>
                <td>14</td>
                <td>15</td>
                <td>15</td>
                <td>16</td>
                <td>16</td>
                <td>17</td>
                <td>17</td>
            </tr>
            <tr class="time head">
                <td>00</td>
                <td>30</td>
                <td>00</td>
                <td>30</td>
                <td>00</td>
                <td>30</td>
                <td>00</td>
                <td>30</td>
                <td>00</td>
                <td>30</td>
                <td>00</td>
                <td>30</td>
                <td>00</td>
                <td>30</td>
                <td>00</td>
                <td>30</td>
                <td>00</td>
                <td>30</td>
                <td>00</td>
                <td>30</td>
            </tr>

            <tr>
                <td class="head">Mon</td>
             <?php                          
             $count =1;         
                    foreach ($arr_days as $key) {
                        if($key == 'MTh' || $key == 'Mo')
                        {
                               $arr[] = $arr_t[$count];
                        }
                            $count++;
                    }
                    $arr_new = $arr;
                    sort($arr_new);
                    //var_dump($arr_new);
                    //var_dump($arr);
                    $count = 1;
                    
                    
                    /*
                    for($i = 0 ; $i< count($arr); $i++)
                    {
                        print($arr_new[$i]).'</br>';
                        
                    }
                    */
                
                for($i = 0 ; $i< count($arr_t); $i++)
                    {
                        $arr_time = explode(' - ', $arr_t[$i]);
                        
                    }
                $count=1;
                
                $arr_subday = array();
                    foreach ($arr_days as $key) {
                        if($key == 'MTh' || $key == 'Mo')
                        {
                           array_push($arr_subday,$arr_s[$count]);
                           // echo $arr_s[$count].'</br>';
                        }
                    
                        $count++;
                    }
                $count=1;
                $table = 20;
                $default = "0800";
                $done = 0;
                $now =0;
                $arr_timeday = array();
                    foreach ($arr_days as $key) {
                        if($key == 'MTh' || $key == 'Mo')
                        {
                            $arr_time = explode(' - ', $arr_t[$count]);
                           array_push($arr_timeday,$arr_time[0]);
                            //echo $arr_t[$count].'</br>';
                        }
                    
                        $count++;
                    }
                
                $result = array_combine($arr_subday, $arr_timeday);
               // print_r($result);
                //var_dump($result).'</br>';
                asort($result);
                /*show
                foreach($result as $x => $x_value) {
                echo  $x . " "  . $x_value;
                echo "<br>";
                }*/
                foreach($result as $x => $x_value) {    
                //echo  $x_value. " "  . $x;
                //echo "<br>";
                 for($i=0;$i<1800;$i+=10){
                     if($x_value == $i){
                         if($done == 0){  
                             
                         if($i % 100 != 0 )
                         {
                             $i+=20;
                         }
                         $now = $i;
                         $show = $i - $default;
                         $td = $show/50;
                         
                        // echo $show;
                         //echo $td;
                             $j=0;
                         for ($j=0; $j < $td; $j++) {
                             echo '<td></td>';
                         
                         }
                         echo '<td class="td">'.$x.'</td>';
                         }
                         
                         else {
                           //  echo $done.'|| </br>';
                            
                             //echo  $now.'</br>';
                             if($i % 100 != 0 )
                                {
                             $i+=20;
                                }
                             
                             // echo $i.'</br>';
                             $show = $i-$now;
                             $now = $i;
                             //echo $show.'</br>';
                             $td = $show/50;
                             //echo $td.'</br>';
                             //echo $td-$done.'</br>';
                            $j=0;
                             for ($j=0; $j < $td-1; $j++) {
                                
                             echo '<td></td>';
                         }
                          
                         echo '<td class="td">'.$x.'</td>';
                         }
                         
                     }
                    }
                    $done++;
                }
                
             ?>

            </tr>
            <tr class="even">
                <td class="head">Tue</td>
                <?php                          
             $count =1;         
                    foreach ($arr_days as $key) {
                        if($key == 'TuF' || $key == 'Tu')
                        {
                               $arr[] = $arr_t[$count];
                        }
                            $count++;
                    }
                    $arr_new = $arr;
                    sort($arr_new);
                    //var_dump($arr_new);
                    //var_dump($arr);
                    $count = 1;
                    
                    
                    /*
                    for($i = 0 ; $i< count($arr); $i++)
                    {
                        print($arr_new[$i]).'</br>';
                        
                    }
                    */
                
                for($i = 0 ; $i< count($arr_t); $i++)
                    {
                        $arr_time = explode(' - ', $arr_t[$i]);
                        
                    }
                $count=1;
                
                $arr_subday = array();
                    foreach ($arr_days as $key) {
                        if($key == 'TuF' || $key == 'Tu')
                        {
                           array_push($arr_subday,$arr_s[$count]);
                           // echo $arr_s[$count].'</br>';
                        }
                    
                        $count++;
                    }
                $count=1;
                $table = 20;
                $default = "0800";
                $done = 0;
                $now =0;
                $arr_timeday = array();
                    foreach ($arr_days as $key) {
                        if($key == 'TuF' || $key == 'Tu')
                        {
                            $arr_time = explode(' - ', $arr_t[$count]);
                           array_push($arr_timeday,$arr_time[0]);
                            //echo $arr_t[$count].'</br>';
                        }
                    
                        $count++;
                    }
                
                $result = array_combine($arr_subday, $arr_timeday);
               // print_r($result);
                //var_dump($result).'</br>';
                asort($result);
                /*show
                foreach($result as $x => $x_value) {
                echo  $x . " "  . $x_value;
                echo "<br>";
                }*/
                foreach($result as $x => $x_value) {    
                //echo  $x_value. " "  . $x;
                //echo "<br>";
                 for($i=0;$i<1800;$i+=10){
                     if($x_value == $i){
                         if($done == 0){  
                             
                         if($i % 100 != 0 )
                         {
                             $i+=20;
                         }
                         $now = $i;
                         $show = $i - $default;
                         $td = $show/50;
                         
                        // echo $show;
                         //echo $td;
                             $j=0;
                         for ($j=0; $j < $td; $j++) {
                             echo '<td></td>';
                         
                         }
                         echo '<td class="td">'.$x.'</td>';
                         }
                         
                         else {
                             //echo $done.'|| </br>';
                            
                             //echo  $now.'</br>';
                             if($i % 100 != 0 )
                                {
                             $i+=20;
                                }
                             
                              //echo $i.'</br>';
                             $show = $i-$now;
                             $now = $i;
                             //echo $show.'</br>';
                             $td = $show/50;
                             //echo $td.'</br>';
                             //echo $td-$done.'</br>';
                            $j=0;
                             for ($j=0; $j < $td-1; $j++) {
                                
                             echo '<td></td>';
                         }
                          
                         echo '<td class="td">'.$x.'</td>';
                         }
                         
                     }
                    }
                    $done++;
                }
                
             ?>
            </tr>
            <tr>
                <td class="head">Wed</td>
                  <?php                          
             $count =1;         
                    foreach ($arr_days as $key) {
                        if($key == 'Wed' || $key == 'We')
                        {
                               $arr[] = $arr_t[$count];
                        }
                            $count++;
                    }
                    $arr_new = $arr;
                    sort($arr_new);
                    //var_dump($arr_new);
                    //var_dump($arr);
                    $count = 1;
                    
                    
                    /*
                    for($i = 0 ; $i< count($arr); $i++)
                    {
                        print($arr_new[$i]).'</br>';
                        
                    }
                    */
                
                for($i = 0 ; $i< count($arr_t); $i++)
                    {
                        $arr_time = explode(' - ', $arr_t[$i]);
                        
                    }
                $count=1;
                
                $arr_subday = array();
                    foreach ($arr_days as $key) {
                        if($key == 'Wed' || $key == 'We')
                        {
                           array_push($arr_subday,$arr_s[$count]);
                           // echo $arr_s[$count].'</br>';
                        }
                    
                        $count++;
                    }
                $count=1;
                $table = 20;
                $default = "0800";
                $done = 0;
                $now =0;
                $arr_timeday = array();
                    foreach ($arr_days as $key) {
                        if($key == 'Wed' || $key == 'We')
                        {
                            $arr_time = explode(' - ', $arr_t[$count]);
                           array_push($arr_timeday,$arr_time[0]);
                            //echo $arr_t[$count].'</br>';
                        }
                    
                        $count++;
                    }
                
                $result = array_combine($arr_subday, $arr_timeday);
               // print_r($result);
                //var_dump($result).'</br>';
                asort($result);
                /*show
                foreach($result as $x => $x_value) {
                echo  $x . " "  . $x_value;
                echo "<br>";
                }*/
                foreach($result as $x => $x_value) {    
                //echo  $x_value. " "  . $x;
                //echo "<br>";
                 for($i=0;$i<1800;$i+=10){
                     if($x_value == $i){
                         if($done == 0){  
                             
                         if($i % 100 != 0 )
                         {
                             $i+=20;
                         }
                         $now = $i;
                         $show = $i - $default;
                         $td = $show/50;
                         
                        // echo $show;
                         //echo $td;
                             $j=0;
                         for ($j=0; $j < $td; $j++) {
                             echo '<td></td>';
                         
                         }
                         echo '<td class="td">'.$x.'</td>';
                         }
                         
                         else {
                             //echo $done.'|| </br>';
                            
                             //echo  $now.'</br>';
                             if($i % 100 != 0 )
                                {
                             $i+=20;
                                }
                             
                              //echo $i.'</br>';
                             $show = $i-$now;
                             $now = $i;
                             //echo $show.'</br>';
                             $td = $show/50;
                             //echo $td.'</br>';
                             //echo $td-$done.'</br>';
                            $j=0;
                             for ($j=0; $j < $td-1; $j++) {
                                
                             echo '<td></td>';
                         }
                          
                         echo '<td class="td">'.$x.'</td>';
                         }
                         
                     }
                    }
                    $done++;
                }
                
             ?>
            </tr>
            <tr class="even">
                <td class="head">Thu</td>
                  <?php                          
             $count =1;         
                    foreach ($arr_days as $key) {
                        if($key == 'MTh' || $key == 'Thu')
                        {
                               $arr[] = $arr_t[$count];
                        }
                            $count++;
                    }
                    $arr_new = $arr;
                    sort($arr_new);
                    //var_dump($arr_new);
                    //var_dump($arr);
                    $count = 1;
                    
                    
                    /*
                    for($i = 0 ; $i< count($arr); $i++)
                    {
                        print($arr_new[$i]).'</br>';
                        
                    }
                    */
                
                for($i = 0 ; $i< count($arr_t); $i++)
                    {
                        $arr_time = explode(' - ', $arr_t[$i]);
                        
                    }
                $count=1;
                
                $arr_subday = array();
                    foreach ($arr_days as $key) {
                        if($key == 'MTh' || $key == 'Thu')
                        {
                           array_push($arr_subday,$arr_s[$count]);
                           // echo $arr_s[$count].'</br>';
                        }
                    
                        $count++;
                    }
                $count=1;
                $table = 20;
                $default = "0800";
                $done = 0;
                $now =0;
                $arr_timeday = array();
                    foreach ($arr_days as $key) {
                        if($key == 'MTh' || $key == 'Thu')
                        {
                            $arr_time = explode(' - ', $arr_t[$count]);
                           array_push($arr_timeday,$arr_time[0]);
                            //echo $arr_t[$count].'</br>';
                        }
                    
                        $count++;
                    }
                
                $result = array_combine($arr_subday, $arr_timeday);
               // print_r($result);
                //var_dump($result).'</br>';
                asort($result);
                /*show
                foreach($result as $x => $x_value) {
                echo  $x . " "  . $x_value;
                echo "<br>";
                }*/
                foreach($result as $x => $x_value) {    
                //echo  $x_value. " "  . $x;
                //echo "<br>";
                 for($i=0;$i<1800;$i+=10){
                     if($x_value == $i){
                         if($done == 0){  
                             
                         if($i % 100 != 0 )
                         {
                             $i+=20;
                         }
                         $now = $i;
                         $show = $i - $default;
                         $td = $show/50;
                         
                        // echo $show;
                         //echo $td;
                             $j=0;
                         for ($j=0; $j < $td; $j++) {
                             echo '<td></td>';
                         
                         }
                         echo '<td class="td">'.$x.'</td>';
                         }
                         
                         else {
                             //echo $done.'|| </br>';
                            
                             //echo  $now.'</br>';
                             if($i % 100 != 0 )
                                {
                             $i+=20;
                                }
                             
                              //echo $i.'</br>';
                             $show = $i-$now;
                             $now = $i;
                             //echo $show.'</br>';
                             $td = $show/50;
                             //echo $td.'</br>';
                             //echo $td-$done.'</br>';
                            $j=0;
                             for ($j=0; $j < $td-1; $j++) {
                                
                             echo '<td></td>';
                         }
                          
                         echo '<td class="td">'.$x.'</td>';
                         }
                         
                     }
                    }
                    $done++;
                }
                
             ?>
            </tr>
            <tr>
                <td class="head">Fri</td>
                  <?php                          
             $count =1;         
                    foreach ($arr_days as $key) {
                        if($key == 'TuF' || $key == 'Fri')
                        {
                               $arr[] = $arr_t[$count];
                        }
                            $count++;
                    }
                    $arr_new = $arr;
                    sort($arr_new);
                    //var_dump($arr_new);
                    //var_dump($arr);
                    $count = 1;
                    
                    
                    /*
                    for($i = 0 ; $i< count($arr); $i++)
                    {
                        print($arr_new[$i]).'</br>';
                        
                    }
                    */
                
                for($i = 0 ; $i< count($arr_t); $i++)
                    {
                        $arr_time = explode(' - ', $arr_t[$i]);
                        
                    }
                $count=1;
                
                $arr_subday = array();
                    foreach ($arr_days as $key) {
                        if($key == 'TuF' || $key == 'Fri')
                        {
                           array_push($arr_subday,$arr_s[$count]);
                           // echo $arr_s[$count].'</br>';
                        }
                    
                        $count++;
                    }
                $count=1;
                $table = 20;
                $default = "0800";
                $done = 0;
                $now =0;
                $arr_timeday = array();
                    foreach ($arr_days as $key) {
                        if($key == 'TuF' || $key == 'Fri')
                        {
                            $arr_time = explode(' - ', $arr_t[$count]);
                           array_push($arr_timeday,$arr_time[0]);
                            //echo $arr_t[$count].'</br>';
                        }
                    
                        $count++;
                    }
                
                $result = array_combine($arr_subday, $arr_timeday);
               // print_r($result);
                //var_dump($result).'</br>';
                asort($result);
                /*show
                foreach($result as $x => $x_value) {
                echo  $x . " "  . $x_value;
                echo "<br>";
                }*/
                foreach($result as $x => $x_value) {    
                //echo  $x_value. " "  . $x;
                //echo "<br>";
                 for($i=0;$i<1800;$i+=10){
                     if($x_value == $i){
                         if($done == 0){  
                             
                         if($i % 100 != 0 )
                         {
                             $i+=20;
                         }
                         $now = $i;
                         $show = $i - $default;
                         $td = $show/50;
                         
                        // echo $show;
                         //echo $td;
                             $j=0;
                         for ($j=0; $j < $td; $j++) {
                             echo '<td></td>';
                         
                         }
                         echo '<td class="td">'.$x.'</td>';
                         }
                         
                         else {
                             //echo $done.'|| </br>';
                            
                             //echo  $now.'</br>';
                             if($i % 100 != 0 )
                                {
                             $i+=20;
                                }
                             
                              //echo $i.'</br>';
                             $show = $i-$now;
                             $now = $i;
                             //echo $show.'</br>';
                             $td = $show/50;
                             //echo $td.'</br>';
                             //echo $td-$done.'</br>';
                            $j=0;
                             for ($j=0; $j < $td-1; $j++) {
                                
                             echo '<td></td>';
                         }
                          
                         echo '<td class="td">'.$x.'</td>';
                         }
                         
                     }
                    }
                    $done++;
                }
                
             ?>
            </tr>
        </table>

    </body>
