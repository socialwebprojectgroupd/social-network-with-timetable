<?php
  class UsersController {
    public function profile() {
      // we store all the posts in a variable
      $users = Users::find($_GET['id_a']);
      $is_friends = Users::is_friend($_GET['id_u'],$_GET['id_a']);
      $alert = Users::alert($_GET['id_u']);
      require_once('views/nav.php');
      require_once('views/profile/profile.php');
    }
    public function add() {
      $users = Users::add($_GET['id_u'],$_GET['id_a']);
      $is_friends = Users::is_friend($_GET['id_u'],$_GET['id_a']);
      $users = Users::find($_GET['id_a']);
      $alert = Users::alert($_GET['id_u']);
      require_once('views/nav.php');
      require_once('views/profile/profile.php');
    }

    public function delete() {
      $users = Users::delete($_GET['id_u'],$_GET['id_a']);
      $is_friends = Users::is_friend($_GET['id_u'],$_GET['id_a']);
      $users = Users::find($_GET['id_a']);
      $alert = Users::alert($_GET['id_u']);
      require_once('views/nav.php');
      require_once('views/profile/profile.php');
    }
    public function confirm()
    {
      $confirm = Users::confirm($_GET['id_u'],$_GET['id_a']);
      header('Location: ?controller=users&action=profile&id_u='.$_GET['id_u'].'&id_a='.$_GET['id_a'].'');
    }
  }
?>