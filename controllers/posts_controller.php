<?php
  class PostsController {

    public function confirm()
    {
      $confirm = Users::confirm($_GET['id_u'],$_GET['id_a']);
      header('Location: ?controller=posts&action=index&id_u='.$_GET['id_u'].'&id_a='.$_GET['id_a'].'');
    }
    public function index() {
      // we store all the posts in a variable
      $posts = Post::all();
      $comment = Comments::all();
      $user = Users::all();
      $alert = Users::alert($_GET['id_u']);
      //var_dump($alert);
      require_once('views/nav.php');
      require_once('views/posts/index.php');
    }

    public function show() {
      // we expect a url of form ?controller=posts&action=show&id=x
      // without an id we just redirect to the error page as we need the post id to find it in the database
      if (!isset($_GET['id']))
        return call('pages', 'error');

      // we use the given id to get the right post
      $post = Post::find($_GET['id']);
      require_once('views/posts/show.php');
    }
  }
?>